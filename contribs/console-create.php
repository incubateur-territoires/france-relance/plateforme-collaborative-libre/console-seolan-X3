#!/usr/bin/php-seolan10
<?php
include('lib2.php');

$homename = myReadline('Unix user name', getenv('USERNAME'));
$root = "/home/$homename/";
if(!file_exists($root)) {
  echo "Le path $root n'existe pas\r\n";
  die();
}

echo "\r\nBase de donnée\r\n";
$user = myReadline('User', $homename);
$pass = myReadline('Password', '');
$host = myReadline('Hostname', '');
$tdb = myReadline('DB Name', $user);

echo "\r\nLangues - Appuyez 2 fois sur entrée pour terminer la saisie (Liste des langues possibles : 'AA','AT','AU','AR','BF','BN','BY','BG','CA','CF','CG','CZ','CN','CR','DE','DK','ES','FI','FR','GB','GE','EL','HU','IR','IT','IV','JP','LB','LV','MK','NO','NL','NZ','PL','PT','RO','RU','SB','SE','SF','SG','SK','SL','SP','TR','US','VI')\r\n";
$langs = [];
$lang = 'empty';
while($lang != '' || count($langs) == 0) {
  $lang = readline("Lang > ");
  if($lang && in_array($lang, array('AA','AT','AU','AR','BF','BN','BY','BG','CA','CF','CG','CZ','CN','CR','DE','DK','ES','FI','FR','GB','GE','EL','HU','IR','IT','IV','JP','LB','LV','MK','NO','NL','NZ','PL','PT','RO','RU','SB','SE','SF','SG','SK','SL','SP','TR','US','VI'))) {
    $langs[] = $lang;
  }
}

echo "\r\n";
$siteurl = myReadline('Adresse du site ($HOME_ROOT_URL)', '');


echo "\r\nCréation des fichiers/dossier\r\n";

$required_path = array(
  "$root/tzr/",
  "$root/www/",
  "$root/www/data/",
  "$root/logs/",
  "$root/var/",
  "$root/var/logs/",
  "$root/var/tmp/"
);
foreach($required_path as $path) {
  if(!file_exists($path)) {
    mkdir($path);
  }
}

$console_path = realpath(__DIR__.'/..');
$links = array(
  $root."www/csx" => $console_path,
  $root."www/scripts" => 'csx/scripts/',
  $root."www/admin" => 'csx/admin/',
  $root."www/index.php" => 'csx/scripts/index.php',
  $root."www/json.php" => 'csx/scripts/json.php',
);
chdir($root."www");
foreach($links as $from => $to) {
  if(!file_exists($from)) {
    symlink($to, $from);
  }
}

echo "\r\nRécupération des données\r\n";

exec("rsync -az $console_path/contribs/console-create/data/ $root/www/data/");
exec("cp $console_path/contribs/console-create/local.ini $root/tzr/local.ini");
$localphp = file_get_contents("$console_path/contribs/console-create/local.php");
$localphp = sprintf($localphp, $user, $pass, $host, $tdb, $langs[0], $langs[0], $console_path.'/', $siteurl, implode('" => "fr", "', $langs) . '" => "fr');
file_put_contents("$root/tzr/local.php", $localphp);
exec("gunzip < $console_path/contribs/console-create/default_dump.sql.gz | mysql -u$user -p$pass -h$host $tdb");

echo "\r\nDone !\r\n";

?>
