<?php
$DATABASE_USER = "%s";
$DATABASE_PASSWORD = "%s";
$DATABASE_HOST = "%s";
$DATABASE_NAME = "%s";
$LANG_DATA = "%s";
$LANG_USER = "%s";
$LIBTHEZORRO = "%s";
$HOME_ROOT_URL = "%s";

$TZR_LANGUAGES = array("%s");

if(file_exists(dirname(__FILE__) . '/local_local.php')) {
  include_once('local_local.php');
}

define('TZR_USE_APP', 1);
define('HTML5MEDIA', true);

$DEBUG_IPs = ['127.0.0.1', '82.64.91.118'];
if (in_array($_SERVER['REMOTE_ADDR'], $DEBUG_IPs) || in_array($_SERVER["HTTP_X_REAL_IP"], $DEBUG_IPs)) {
  define('TZR_DEBUG_MODE', E_ALL & ~E_DEPRECATED & ~E_NOTICE & ~E_STRICT & ~E_WARNING);
  define('TZR_LOG_LEVEL', 'PEAR_LOG_DEBUG');
  $TZR_LOG_FILTERS = array('.');
  ini_set('display_errors',1);
}
else {
  define('TZR_DEBUG_MODE', 0);
}
