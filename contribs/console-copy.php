#!php-seolan8 
<?php
include(__DIR__.'lib.inc');

echo "Target : \r\n";
$user=myReadline('User target','');
$pass=myReadline('Password target','');
$host=myReadline('DB Hostname target','');
$tdb=myReadline('DB Name target',$user);

echo "Source : \r\n";
$server=myReadline('Serveur source','web05.xsalto.net');
$ouser=myReadline('User source','corailv38');
$opass=myReadline('Password source','');
$ohost=myReadline('DB Hostname source','web04');
$sdb=myReadline('DB Name source',$ouser);

$localcreate=myConfirm("Create local.inc/.ini?","Y");
$dumpsql=myConfirm("Copy data base ?","Y");
$copyclass=myConfirm("Copy class ?","Y");
$copytemplates=myConfirm("Copy templates ?","Y");
$copydata=myConfirm("Copy data ?","Y");
$copyother=readline('Others files/folders to copy (separeted by space and name relative to home. Ex : images js/home.js...) > '); 

if($copyclass){
  if(!file_exists('/home/'.$user.'/tzr')) mkdir('/home/'.$user.'/tzr');
  echo 'Class copy : password source ('.$opass.'): ';
  system('scp '.$ouser.'@'.$server.':/home/'.$ouser.'/tzr/class.*.inc /home/'.$user.'/tzr/');
}
if($copytemplates){
  if(!file_exists('/home/'.$user.'/www')) mkdir('/home/'.$user.'/www');
  echo 'Templates copy : password source ('.$opass.'): ';
  system('scp -r '.$ouser.'@'.$server.':/home/'.$ouser.'/www/templates/ /home/'.$user.'/www/');
}
if($copydata){
  if(!file_exists('/home/'.$user.'/www')) mkdir('/home/'.$user.'/www');
  echo 'Data copy : password source ('.$opass.'): ';
  system('scp -r '.$ouser.'@'.$server.':/home/'.$ouser.'/www/data/ /home/'.$user.'/www/');
}
if(!empty($copyother)){
  $list=explode(' ',$copyother);
  foreach($list as $f){
    echo 'Other copy "'.$f.'": password source ('.$opass.'): ';
    system('scp -r '.$ouser.'@'.$server.':/home/'.$ouser.'/www/'.$f.' /home/'.$user.'/www/'.$f);
  }
}
if($localcreate) {
  echo "Local.ini/.inc creation : \r\n";
  $defaultlang=myReadline('Default Language','FR');
  $configfile="/home/$user/tzr/local.inc";
  $version=myReadline('Version','seolan8');
  $homerooturl=myReadline('Website Url',"http://www.".$user.".xsalto.com/");
  $start=myReadline('Start Class','CorailV3');
  
  $txt="<?php\ndefine('TZR_IS5',1);\n";
  $txt.='$DATABASE_USER = "'.$user.'";'."\n";
  $txt.='$DATABASE_PASSWORD = "'.$pass.'";'."\n";
  $txt.='$DATABASE_HOST = "'.$host.'";'."\n";
  $txt.='$DATABASE_NAME = "'.$tdb.'";'."\n";
  $txt.='$LANG_DATA = "'.$defaultlang.'";'."\n";
  $txt.='$LANG_USER = "'.$defaultlang.'";'."\n";
  $txt.='$HOME = "'.$user.'";'."\n";
  $txt.='$LIBTHEZORRO = "/home/tzr-master/'.$version.'/";'."\n";
  $txt.='$HOME_ROOT_URL = "'.$homerooturl.'"'.";\n";
  $txt.='$SELF_PREFIX            = "";'."\n";
  $txt.='$START_CLASS            = "'.$start.'";'."\n";
  $txt.='$TZR_LANGUAGES=array("'.$defaultlang.'"=>"'.$defaultlang.'");'."\n";
  $txt.='session_cache_limiter(\'private, must-revalidate\');';
  $txt.="\n?>\n";
  $base=dirname($configfile);
  if(!file_exists($base)) mkdir($base);
  file_put_contents($configfile, $txt);

  $societe=myReadline('Society');
  echo 'Local.ini copy : password source ('.$opass.'): ';
  system('scp '.$ouser.'@'.$server.':/home/'.$ouser.'/tzr/local.ini /home/'.$user.'/tzr/local.ini');

  $a=file_get_contents('/home/'.$user.'/tzr/local.ini');
  $a=preg_replace('/societe = ".+"/','societe = "'.$societe.'"',$a);
  $a=preg_replace('/societe_url = ".+"/','societe_url = "'.$homerooturl.'"',$a);
  file_put_contents('/home/'.$user.'/tzr/local.ini',$a);
}

if($dumpsql){
  echo "Execute mysqldump\n";
  system("mysqldump --add-drop-table -u$ouser -p$opass -h$ohost $sdb|mysql -u$user -p$pass -h$host $tdb");
}
system("ln -s /home/tzr-master/$version/public /home/$user/www/tzr");
system("ln -s tzr/admin /home/$user/www/admin");
system("ln -s tzr/admin /home/$user/www/admin.php");
system("ln -s tzr/scripts/index.php /home/$user/www/index.php");
system("mkdir /home/$user/www/templates_c");
system("mkdir /home/".$user."/var");
system("mkdir /home/".$user."/var/tmp");
system("mkdir /home/".$user."/var/logs");
?>
