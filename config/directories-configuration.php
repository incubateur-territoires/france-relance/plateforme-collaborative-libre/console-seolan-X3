<?php
  /**
   * les annuaires d'utilisateurs
   * local directory et private sont obligatoires et surchargeables dans le tzr/directories-configurations.php, en particulier pour les filtres isQualified et exclusiveUser
   * par defaut : 
   * tout login est "exclusif" à l'annuaire local et ne l'est sur private
   * les logins en xsalto.com sont d'abord contrôlés sur private
   */
return [
	// k = id dans la conf qui suit, v = ordre de préséance
	// ordre = false pour désactiver un des annuaires par défaut
	'directoriesId'=>['local'=>2,'xsalto'=>1],
	'local'=>['classname'=>'\Seolan\Core\Directory\LocalDirectory',
		  'label'=>'Annuaire local',
		  'config'=>['loginFilter'=>'/^\S*$/',
			     'exclusiveFilter'=>'/^(.*)$/'
			     ]
		  ],
	'xsalto'=>['classname'=>'\Seolan\Core\Directory\RemoteDirectory',
		   'label'=>'XSALO - private',
		   'disableautosync'=>true,
		   'config'=>['localusergroup'=>['GRP:1'],
			      'loginFilter'=>'/^(.+\@xsalto\.com)$/',
			      'exclusiveFilter'=>'/^!(.*)$/',
			      'localusergroup'=>['GRP:1'],
			      'url'=>'https://private.xsalto.com/csx/scripts-admin/json.php/remoteAuthentication']
		   ]
	];
