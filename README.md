# Console Séolan

La Console Séolan est un CMS Open-Source développé par la société [XSALTO](http://www.xsalto.com/).

## Installation

### Sur un serveur UNIX

Copiez tout d'abord les sources de la façon suivante :
```
cd ~/..
git clone https://git.xsalto.com/console-seolan/console-seolan.git
```

Puis exécutez le script d'installation :
```
php console-seolan/scripts/install/install.php
```

## Documentation

### Utilisateur

La documentation **utilisateur** est disponible dans l'interface même de la Console Séolan (interface que l'on appelle plus communément le **back-office**) grâce à un petit icône (i) situé dans la barre d'action de chaque module.

Les sources de cette documentation sont situées dans le répertoire `console-seolan/public/usermanual/`.

### Développeur et intégrateur

Cette documentation est accessible depuis n'importe quel site via l'URL `[mon-site.com]/tzr/devmanual/html/` (`tzr/` étant un lien symbolique vers le répertoire `console-seolan/public/`).

Les sources de cette documentation sont situées dans le répertoire `console-seolan/doc/source/` la génération des fichiers HTML se fait avec [Sphinx](http://www.sphinx-doc.org/).

### API

La documentation API est accessible depuis n'importe quel site via l'URL `[mon-site.com]/tzr/devmanual/api/` (`tzr/` étant un lien symbolique vers le répertoire `console-seolan/public/`).

Cette documentation est générée par [Doxygen](http://www.doxygen.org/) qui utilise les commentaires des sources PHP de la Console Séolan de la [manière suivante](http://www.doxygen.org/manual/docblocks.html#docexamples).

## Licence

La Console Séolan est distribuée sous [licence GPL](https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU).
